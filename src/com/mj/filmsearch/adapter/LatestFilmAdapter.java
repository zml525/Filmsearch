package com.mj.filmsearch.adapter;

import java.util.List;

import com.mj.filmsearch.R;
import com.mj.filmsearch.activity.FilmDetailActivity;
import com.mj.filmsearch.bean.FilmDto;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * 最新电影
 * @author zhaominglei
 * @date 2015-5-10
 * 
 */
public class LatestFilmAdapter extends BaseAdapter {
	private Context context;
	private List<FilmDto> filmDtos;
	
	public LatestFilmAdapter(Context context, List<FilmDto> filmDtos) {
		super();
		this.context = context;
		this.filmDtos = filmDtos;
	}

	public List<FilmDto> getFilmDtos() {
		return filmDtos;
	}

	public void setFilmDtos(List<FilmDto> filmDtos) {
		this.filmDtos = filmDtos;
	}

	@Override
	public int getCount() {
		return filmDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return filmDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout relativeLayout;
		if (convertView != null) {
			relativeLayout = (RelativeLayout) convertView;
		} else {
			relativeLayout = (RelativeLayout) View.inflate(context,
					R.layout.latestfilm_item, null);
		}
		final FilmDto filmDto = filmDtos.get(position);
		((TextView) relativeLayout.findViewById(R.id.latestfilm_item_name)).setText(filmDto.getName());
		ImageView infoView = (ImageView) relativeLayout.findViewById(R.id.latestfilm_item_info);
		infoView.setTag(filmDto.getName()); 
		relativeLayout.setTag(filmDto.getName());//通过标签确定单击时是否是该对象 整个加上单击事件
		relativeLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String descName = (String)v.getTag();
				if (descName != null && descName.equals(filmDto.getName())) {
					Intent intent = new Intent(v.getContext(), FilmDetailActivity.class);
					intent.putExtra("name", filmDto.getName());
					intent.putExtra("detailURL", filmDto.getDetailURL());
					v.getContext().startActivity(intent);
				}
			}
		});
		return relativeLayout;
	}
}
