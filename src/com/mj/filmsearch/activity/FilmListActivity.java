package com.mj.filmsearch.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.filmsearch.R;
import com.mj.filmsearch.adapter.LatestFilmAdapter;
import com.mj.filmsearch.bean.FilmDto;
import com.mj.filmsearch.bean.FilmList;
import com.mj.filmsearch.service.FilmsearchService;
import com.mj.filmsearch.swipeback.SwipeBackActivity;
import com.mj.filmsearch.util.DateUtils;
import com.mj.filmsearch.util.NetUtils;
import com.mj.filmsearch.widget.LoadingDialog;
import com.mj.filmsearch.widget.XListView;
import com.mj.filmsearch.widget.XListView.IXListViewListener;
/**
 * 电影列表
 * @author zhaominglei
 * @date 2015-5-11
 * 
 */
public class FilmListActivity extends SwipeBackActivity implements	OnClickListener,IXListViewListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = FilmListActivity.class.getSimpleName();
	private FilmsearchService filmsearchService = new FilmsearchService();
	private ImageView goHome;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private LoadingDialog loadingDialog; //加载框
	private XListView filmListView;
	private List<FilmDto> filmDtos = new ArrayList<FilmDto>();
	private LatestFilmAdapter filmAdapter;
	private Button appOffersButton; //推荐应用
	private int page = 0; //页码
	private String keyword; //关键词
	private String searchURL; //检索URL 第一次检索后存入后按这个爬去
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_film_list);
		init();
	}
	
	private void init() {
		goHome = (ImageView)findViewById(R.id.filmlist_gohome);
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.filmlist_miniAdLinearLayout);
		filmListView = (XListView)findViewById(R.id.film_listview);
		filmAdapter = new LatestFilmAdapter(this, filmDtos);
		filmListView.setPullLoadEnable(true);
		filmListView.setXListViewListener(this);
		filmListView.setAdapter(filmAdapter);
		appOffersButton = (Button)findViewById(R.id.filmlist_appOffersButton);
		
		goHome.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		loadingDialog = new LoadingDialog(this);
		
		appControl = AppControl.getInstance();
		appControl.init(FilmListActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		appControl.loadPopAd(FilmListActivity.this);
		appControl.showPopAd(FilmListActivity.this, 60 * 1000);
		appControl.showInter(FilmListActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(FilmListActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
		
		keyword = getIntent().getStringExtra("keyword");
		if (keyword == null || keyword.equals("")) {
			Toast.makeText(getApplicationContext(), R.string.filmsearch_filmname_hint, Toast.LENGTH_SHORT).show();
			FilmListActivity.this.finish();
		}
		getFilmList();
	}

	private void getFilmList() {
		if (NetUtils.isConnected(getApplicationContext())) {
			loadingDialog.show();
			new FilmListAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
			onLoad();
		}
	}
	
	@Override
	public void onRefresh() {
		getFilmList();
	}

	@Override
	public void onLoadMore() {
		getFilmList();
		page++;
	}
	
	//刷新结束
	public void onLoad() {
		filmListView.stopRefresh();
		filmListView.stopLoadMore();
		filmListView.setRefreshTime(DateUtils.format(new Date(), DateUtils.datePattern));
	}	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.filmlist_gohome:
			FilmListActivity.this.finish();
			break;
		
		case R.id.filmlist_appOffersButton:
			access.openWALL(FilmListActivity.this);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		FilmListActivity.this.finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	public class FilmListAsyncTask extends AsyncTask<String, String, FilmList> {
		@Override
		protected FilmList doInBackground(String... params) {
			return filmsearchService.getFilmList(getApplicationContext(), keyword, page, searchURL);
		}

		@Override
		protected void onPostExecute(FilmList result) {
			loadingDialog.dismiss();
			if (result != null && result.getRecordList() != null && result.getRecordList().size() > 0) {
				searchURL = result.getSearchURL();
				filmAdapter.setFilmDtos(result.getRecordList());
				filmAdapter.notifyDataSetChanged();
				onLoad();
			}
		}
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
