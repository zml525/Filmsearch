package com.mj.filmsearch.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.filmsearch.R;
import com.mj.filmsearch.adapter.LatestFilmAdapter;
import com.mj.filmsearch.bean.FilmDto;
import com.mj.filmsearch.service.FilmsearchService;
import com.mj.filmsearch.util.NetUtils;
import com.mj.filmsearch.widget.LoadingDialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
/**
 * 电影搜索
 * 
 * @author zhaominglei
 * @date 2015-5-9
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	public static final String MUMAYI_APPID = "f1706e066072b4c9bbVm1KSHTJ8jZ6NkChJslxVrG7zNBoibPdhLvVVEdn+6jjuSeA"; //app_id
	public static final String MUMAYI_APPCHANNEL = "木蚂蚁"; //app_channel
	private FilmsearchService filmsearchService = new FilmsearchService();
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private EditText filmnameText; //电影名称输入框
	private String filmname; //电影名称
	private Button searchBtn; //搜索
	private LoadingDialog loadingDialog; // 加载框
	private ListView latestView; //最新电影
	private LatestFilmAdapter latestFilmAdapter;
	private List<FilmDto> filmDtos = new ArrayList<FilmDto>();
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		filmnameText = (EditText)findViewById(R.id.filmsearch_filmname_edt);
		searchBtn = (Button)findViewById(R.id.filmsearch_search_btn);
		latestView = (ListView)findViewById(R.id.filmsearch_latest_listview);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		searchBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		latestFilmAdapter = new LatestFilmAdapter(this, filmDtos);
		latestView.setAdapter(latestFilmAdapter);
		loadingDialog = new LoadingDialog(this);
		
		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, MUMAYI_APPID, MUMAYI_APPCHANNEL);
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, MUMAYI_APPID, MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
		
		getLatestFilm();
	}
	
	private void getLatestFilm() {
		if (NetUtils.isConnected(getApplicationContext())) {
			loadingDialog.show();
			new LatestFilmAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.filmsearch_search_btn:
			filmname = filmnameText.getText().toString();
			if (filmname == null || filmname.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.filmsearch_filmname_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			
			Intent intent = new Intent(MainActivity.this, FilmListActivity.class);
			intent.putExtra("keyword", filmname);
			startActivity(intent);
			break;
			
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	public class LatestFilmAsyncTask extends AsyncTask<String, String, List<FilmDto>> {
		@Override
		protected List<FilmDto> doInBackground(String... params) {
			return filmsearchService.getLatestFilm(getApplicationContext());
		}

		@Override
		protected void onPostExecute(List<FilmDto> result) {
			loadingDialog.dismiss();
			if (result != null && result.size() > 0) {
				latestFilmAdapter.setFilmDtos(result);
				latestFilmAdapter.notifyDataSetChanged();
			}
		}
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}
}
