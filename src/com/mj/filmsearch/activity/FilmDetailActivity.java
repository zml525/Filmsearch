package com.mj.filmsearch.activity;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.filmsearch.R;
import com.mj.filmsearch.bean.FilmDto;
import com.mj.filmsearch.service.FilmsearchService;
import com.mj.filmsearch.util.NetUtils;
import com.mj.filmsearch.widget.LoadingDialog;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ArrowKeyMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;
/**
 * 详情页
 * @author zhaominglei
 * @date 2015-5-10
 */
public class FilmDetailActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = FilmDetailActivity.class.getSimpleName();
	private FilmsearchService filmsearchService = new FilmsearchService();
	private ImageView goHome;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private LoadingDialog loadingDialog; // 加载框
	private TextView nameView; //名称
	private EditText resultView; //下载地址
	private Button appOffersButton; //推荐应用
	private String name; //电影名称
	private String detailURL; //电影详情页地址
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_film_detail);
		init();
	}

	private void init() {
		goHome = (ImageView)findViewById(R.id.filmdetail_gohome);
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.filmdetail_miniAdLinearLayout);
		nameView = (TextView)findViewById(R.id.filmdetail_name);
		resultView = (EditText)findViewById(R.id.filmdetail_result);
		appOffersButton = (Button)findViewById(R.id.filmdetail_appOffersButton);
		
		goHome.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		loadingDialog = new LoadingDialog(this);
		//利用 EditText代替 TextView实现自由复制
		//参考：http://blog.csdn.net/jaycee110905/article/details/8762274
		resultView.setFocusableInTouchMode(true);
		resultView.setFocusable(true);
		resultView.setLongClickable(true);
		resultView.setSingleLine(false);
		resultView.setMovementMethod(ArrowKeyMovementMethod.getInstance());
		
		appControl = AppControl.getInstance();
		appControl.init(FilmDetailActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		appControl.loadPopAd(FilmDetailActivity.this);
		appControl.showPopAd(FilmDetailActivity.this, 60 * 1000);
		appControl.showInter(FilmDetailActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(FilmDetailActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
		
		name = getIntent().getStringExtra("name");
		detailURL = getIntent().getStringExtra("detailURL");
		if (name == null || name.equals("") || detailURL == null || detailURL.equals("")) {
			Toast.makeText(getApplicationContext(), R.string.data_error, Toast.LENGTH_SHORT).show();
			FilmDetailActivity.this.finish();
		}
		nameView.setText(name);		
		getFilmDetail();
	}

	private void getFilmDetail() {
		if (NetUtils.isConnected(getApplicationContext())) {
			loadingDialog.show();
			new FilmDetailAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.filmdetail_gohome:
			FilmDetailActivity.this.finish();
			break;
			
		case R.id.appOffersButton:
			access.openWALL(FilmDetailActivity.this);
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		FilmDetailActivity.this.finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	public class FilmDetailAsyncTask extends AsyncTask<String, String, FilmDto> {
		@Override
		protected FilmDto doInBackground(String... params) {
			return filmsearchService.getFilmDetail(getApplicationContext(), name, detailURL);
		}

		@Override
		protected void onPostExecute(FilmDto result) {
			loadingDialog.dismiss();
			if (result != null) {
				StringBuilder detail = new StringBuilder();
				StringBuilder sb = new StringBuilder();
				if (result.getDownList() != null && result.getDownList().size() > 0) {
					for (String downloadURL : result.getDownList()) {
						sb.append(downloadURL).append("\n\n");
					}
				}
				detail.append("下载地址:\n\n").append(sb.toString()).append("\n\n");
				String summary = result.getSummary();
				summary = summary.replaceAll("<br />", "\n");
				summary = summary.replaceAll("&middot;", "·");
				detail.append(summary);
				resultView.setText(detail.toString(), BufferType.SPANNABLE);
			}
		}
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
