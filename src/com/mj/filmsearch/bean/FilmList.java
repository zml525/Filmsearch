package com.mj.filmsearch.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 电影列表
 * @author zhaominglei
 * @date 2015-5-11
 * 
 */
public class FilmList implements Serializable {
	private static final long serialVersionUID = 6548436489398503914L;
	private int currentPage; //当前页
    private int pageSize; //每页显示记录数
    private long recordCount; //总记录数
    private List<FilmDto> recordList; //记录集合
    private int pageCount; //总页数
    private int prePage; //上一页
    private int nextPage; //下一页
    private boolean hasPrePage; //是否有上一页
    private boolean hasNextPage; //是否有下一页
    private String searchURL; //检索URL 第一次检索后存入后按这个爬去
    
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public long getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
	}
	public List<FilmDto> getRecordList() {
		return recordList;
	}
	public void setRecordList(List<FilmDto> recordList) {
		this.recordList = recordList;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getPrePage() {
		return prePage;
	}
	public void setPrePage(int prePage) {
		this.prePage = prePage;
	}
	public int getNextPage() {
		return nextPage;
	}
	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}
	public boolean isHasPrePage() {
		return hasPrePage;
	}
	public void setHasPrePage(boolean hasPrePage) {
		this.hasPrePage = hasPrePage;
	}
	public boolean isHasNextPage() {
		return hasNextPage;
	}
	public void setHasNextPage(boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}
	public String getSearchURL() {
		return searchURL;
	}
	public void setSearchURL(String searchURL) {
		this.searchURL = searchURL;
	}
}
