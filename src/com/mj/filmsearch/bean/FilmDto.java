package com.mj.filmsearch.bean;

import java.io.Serializable;
import java.util.List;
/**
 * 电影dto
 * @author zhaominglei
 * @date 2015-5-10
 * 
 */
public class FilmDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name; //名称
	private String detailURL; //详情页地址
	private String summary; //简介
	private List<String> downList; //下载地址
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetailURL() {
		return detailURL;
	}
	public void setDetailURL(String detailURL) {
		this.detailURL = detailURL;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public List<String> getDownList() {
		return downList;
	}
	public void setDownList(List<String> downList) {
		this.downList = downList;
	}
}
