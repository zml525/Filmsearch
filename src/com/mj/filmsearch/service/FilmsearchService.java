package com.mj.filmsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mj.filmsearch.bean.FilmDto;
import com.mj.filmsearch.bean.FilmList;
import com.mj.filmsearch.util.HttpUtils;
import com.mj.filmsearch.util.RegUtil;
import com.mj.filmsearch.util.SPUtils;

import android.content.Context;
/**
 * 电影搜索
 * @author zhaominglei
 * @date 2015-5-10
 * 
 */
public class FilmsearchService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = FilmsearchService.class.getSimpleName();
	private static final String FILM_INDEX_URL = "http://www.6vhao.com"; //6vhao
	private static final String FILM_SEARCH_URL = "http://www.6vhao.com/e/search/index.php"; //6vhao
	private static final String FILM_SEARCH_PAGE_URL = "http://www.6vhao.com/e/search/result/index.php"; //6vhao
	
	public void syncLatestFilm(Context context) {
		String html = HttpUtils.doGetFor6vhao(FILM_INDEX_URL, "gbk");
		if (html != null && !html.equals("")) {
			Document document = Jsoup.parse(html);
			Elements ul_els = document.getElementsByClass("lt");
			if (ul_els != null && !ul_els.isEmpty()) {
				SPUtils.clear(context);
				Element ul_el = ul_els.get(0);
				Elements a_els = ul_el.getElementsByTag("a");
				for (Element a_el : a_els) {
					String a_href = a_el.attr("href");
					String a_text = a_el.text();
					
					SPUtils.put(context, a_text, a_href);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FilmDto> getLatestFilm(Context context) {
		Map<String, String> map = (Map<String, String>) SPUtils.getAll(context);
		if (map == null || map.isEmpty()) {
			syncLatestFilm(context);
		}
		map = (Map<String, String>) SPUtils.getAll(context);
		List<FilmDto> filmDtos = new ArrayList<FilmDto>();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			
			FilmDto dto = new FilmDto();
			dto.setName(key);
			dto.setDetailURL(value);
			filmDtos.add(dto);
		}
		return filmDtos;
	}
	
	public FilmDto getFilmDetail(Context context, String name, String detailURL) {
		if (detailURL == null || detailURL.equals("")) {
			return null;
		}
		String html = HttpUtils.doGetFor6vhao(detailURL, "gbk");
		if (html != null && !html.equals("")) {
			FilmDto dto = new FilmDto();
			dto.setName(name);
			dto.setDetailURL(detailURL);
			Document document = Jsoup.parse(html);
			Elements elements = document.getElementsByTag("table");
			if (elements != null && !elements.isEmpty()) {
				List<String> downList = new ArrayList<String>();
				Element element = elements.last();
				Elements tr_els = element.getElementsByTag("tr");
				for (Element tr_el : tr_els) {
					String text = tr_el.text();
					String url = "";
					Elements a_els = tr_el.getElementsByTag("a");
					if (a_els != null && !a_els.isEmpty()) {
						Element a_el = a_els.get(0);
						url = a_el.attr("href");
					}
					String downloadURL = "";
					if (text.indexOf(url) > -1) {
						downloadURL = text;
					} else {
						downloadURL = text + "\n" + url;
					}
					downList.add(downloadURL);
				}
				dto.setDownList(downList);
			}
			Elements meta_els = document.getElementsByAttributeValue("name", "description");
			if (meta_els != null && !meta_els.isEmpty()) {
				Element summary_el = meta_els.get(0);
				String summary = summary_el.attr("content");
				if (summary != null && summary.indexOf("【下载地址】") > -1) {
					summary = summary.substring(0, summary.indexOf("【下载地址】"));
				}
				dto.setSummary(summary);
			}
			return dto;
		}
		return null;
	}
	
	public FilmList getFilmList(Context context, String keyword, int page, String searchURL) {
		String html = "";
		if (searchURL == null || searchURL.equals("")) {
			String param = "show=title%2Csmalltext&tempid=1&keyboard="+HttpUtils.encodeURI(keyword, "gb2312")+"&tbname=article&x=20&y=13";
			html = HttpUtils.doPostFor6vhao(FILM_SEARCH_URL, param, "gbk");
		} else {
			String url = searchURL + "&page=" + page;
			html = HttpUtils.doGetFor6vhao(url, "gbk");
		}
		if (html != null && !html.equals("")) {
			FilmList filmList = new FilmList();
			List<FilmDto> filmDtos = new ArrayList<FilmDto>();
			Document document = Jsoup.parse(html);
			Elements elements = document.getElementsByAttributeValue("class", "blue14");
			if (elements != null && !elements.isEmpty()) {
				for (Element element : elements) {
					Elements a_els = element.getElementsByTag("a");
					if (a_els != null && !a_els.isEmpty()) {
						Element a_el = a_els.get(0);
						FilmDto dto = new FilmDto();
						dto.setName(a_el.text());
						dto.setDetailURL(a_el.attr("href"));
						filmDtos.add(dto);
					}
				}
			}
			String searchId = RegUtil.getMatchRegStr(html, "searchid=(.+?)\"");
			if (searchId != null && !searchId.equals("")) {
				String url = FILM_SEARCH_PAGE_URL + "?searchid="+searchId;
				filmList.setSearchURL(url);
			}
			filmList.setRecordList(filmDtos);
			return filmList;
		}
		return null;
	}
}
