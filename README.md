#Filmsearch

#简介
电影搜索是一款搜索电影资源的软件。输入电影名称点搜索即可显示搜素到的电影资源点击详情即可看到电影的下载地址和简介。资源丰富，你想看的,你不想看的这里都有！

#演示
http://www.wandoujia.com/apps/com.mj.filmsearch

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")